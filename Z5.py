from random import choice, shuffle
words = ["kot", "pies", "papuga", "chomik", "karaluch", "zebra"]


def quiz(words):
    word_to_guess = choice(words)
    print(word_to_guess)
    word_shuffled = ([letter for letter in word_to_guess])
    shuffle(word_shuffled)
    print(" ".join(word_shuffled))
    answer = input("Co to za słowo?  ")
    while answer != word_to_guess:
        if answer == "q" or answer == "Q":
            break
        else:
            answer = input("Spróbuj jeszcze raz ")
    if answer == word_to_guess:
        print("Zgadłeś !")


quiz(words)
