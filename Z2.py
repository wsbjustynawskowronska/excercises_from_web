from random import randint

n = randint(1, 30)
response = int(input("Try to guess number from 1 to 30: "))
while response != n:
    response = int(input("Please, try again "))

print("You are right, the number is ", n)

