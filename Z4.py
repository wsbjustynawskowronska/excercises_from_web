# obliczanie silni z for (ograniczenie do 15):
def exampleone():
    n = int(input(" Podaj dowolną liczbę całkowitą do 15:  "))
    while n < 0 or n > 15:
        n = float(input("Tego nie obliczę. Podaj dowolną liczbę całkowitą od 0 do 15:  "))
    if n == 0:
        print(f"! {int (n)} = 0")
    else:
        s = 1
        n = int(n)
        for _ in range(1, (n + 1)):
            s = s * _
        print(f"!{n} = {s}")


# obliczanie silni z while(ograniczenie do 15):
def exampletwo():
    n = int(input(" Podaj dowolną liczbę całkowitą do 15:  "))
    while n < 0 or n > 15:
        n = float(input("Tego nie obliczę. Podaj dowolną liczbę całkowitą od 0 do 15:  "))
    if n == 0:
        print(f"! {int (n)} = 0")
    s = 1
    nn = int(n)
    if n > 0:
        while n > 0:
            s = s * n
            n = n - 1
        print(f"!{nn} = {s}")


exampleone()
exampletwo()
